
## 0.2.0 [05-22-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/inventory/adapter-menmice_micetro!1

---

## 0.1.1 [07-28-2021]

- Initial Commit

See commit 64260bc

---
