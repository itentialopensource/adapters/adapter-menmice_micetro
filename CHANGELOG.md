
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:26PM

See merge request itentialopensource/adapters/adapter-menmice_micetro!12

---

## 0.4.3 [09-01-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-menmice_micetro!10

---

## 0.4.2 [08-15-2024]

* Changes made at 2024.08.14_18:38PM

See merge request itentialopensource/adapters/adapter-menmice_micetro!9

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_19:50PM

See merge request itentialopensource/adapters/adapter-menmice_micetro!8

---

## 0.4.0 [05-14-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/inventory/adapter-menmice_micetro!7

---

## 0.3.4 [03-26-2024]

* Changes made at 2024.03.26_14:44PM

See merge request itentialopensource/adapters/inventory/adapter-menmice_micetro!6

---

## 0.3.3 [03-13-2024]

* Changes made at 2024.03.13_14:02PM

See merge request itentialopensource/adapters/inventory/adapter-menmice_micetro!5

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_14:11PM

See merge request itentialopensource/adapters/inventory/adapter-menmice_micetro!4

---

## 0.3.1 [02-26-2024]

* Changes made at 2024.02.26_13:40PM

See merge request itentialopensource/adapters/inventory/adapter-menmice_micetro!3

---

## 0.3.0 [12-29-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/inventory/adapter-menmice_micetro!2

---

## 0.2.0 [05-22-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/inventory/adapter-menmice_micetro!1

---

## 0.1.1 [07-28-2021]

- Initial Commit

See commit 64260bc

---
