# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Menmice_micetro System. The API that was used to build the adapter for Menmice_micetro is usually available in the report directory of this adapter. The adapter utilizes the Menmice_micetro API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Men and Mice Micetro adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Men and Mice Micetro. With this adapter you have the ability to perform operations on items such as:

- IPAM
- DHCP
- DNS

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
